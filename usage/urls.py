from django.urls import path
from . import views

urlpatterns = [
    path('', views.usage, name='usage'),
    path('get_cpu_usage', views.get_cpu_usage, name='get_cpu_usage'),
]