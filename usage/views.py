from django.shortcuts import render
import psutil
import subprocess
from django.http import JsonResponse
import time
import json

# Create your views here.

def get_cpu_usage(request):
    cpu_usage = []
    try:
        for i in range(8):
            time.sleep(0.125)
            cpu_usage.append([round(i*0.125,2),round(psutil.cpu_percent(),2)])
    except:
        pass
    return JsonResponse(cpu_usage, safe=False)

def get_memory_usage():
    memory_usage = {}
    try:
        virtual_memory = psutil.virtual_memory()
        memory_usage['total'] = round(virtual_memory.total/(1024.0 ** 2),2)
        memory_usage['used'] = round(memory_usage['total']/100*virtual_memory.percent,2)
        memory_usage['available'] = memory_usage['total']-memory_usage['used']
    except:
        pass
    return memory_usage

def get_disk_usage():
    disk_usage = {}
    try:
        disk_usage_object = psutil.disk_usage('/')
        disk_usage['total'] = round(disk_usage_object.total/(1024.0 ** 3),2)
        disk_usage['used'] = round(disk_usage['total']/100*disk_usage_object.percent,2)
        disk_usage['available'] = disk_usage['total']-disk_usage['used']
    except:
        pass
    return disk_usage

def usage(request):
    memory_usage = get_memory_usage()
    disk_usage = get_disk_usage()
    return render(request, 'usage/index.html', {"memory_usage":memory_usage,"disk_usage":disk_usage})

