# Django web app to monitor System Usage

## Getting Started

Setup project environment  and activate it

Windows
```bash
python -m venv myvenv
myvenv\Scripts\activate
```

Ubuntu /Osx
```bash
python3 -m venv myvenv
source myvenv/bin/activate
```

Now install requirements
```bash
pip install -r requirements.txt
python manage.py runserver
```

Go to http://127.0.0.1:8000/
